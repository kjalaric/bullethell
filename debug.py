from datetime import datetime
import tkinter as tk
import tkinter.font as tkFont
from game import window, game_start, statemgr


class DebugPanel(object):
    def __init__(self):
        self.textid = None
        self.statestackid = None

    def create(self):
        self.textid = window.create_text(10, 10, fill="green", text="Test", anchor=tk.NW, font=tkFont.Font(family="Futura", size=24, weight="normal"))
        self.statestackid = window.create_text(10, 40, fill="green", text="Test", anchor=tk.NW, font=tkFont.Font(family="Futura", size=10, weight="normal"))

    def update(self):
        if self.textid is None:
            self.create()
        else:
            window.itemconfigure(self.textid, text=self.generate_debug_text())
            window.itemconfigure(self.statestackid, text=self.generate_debug_text_statestack())

    def generate_debug_text(self):
        time = datetime.now() - game_start
        state = statemgr.state[-1].name
        return f"{time}: {state}"

    def generate_debug_text_statestack(self):
        return ">" + "\n".join(x.name for x in statemgr.state[::-1])


debug_panel = DebugPanel()
