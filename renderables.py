from defines import *
from game import *
from text_object import Text


class Renderable(object):
    def __init__(self, render_object):
        self.render_object = render_object
        self.hidden = False
        self.drawn = False
        self.trash = False

    def update(self, dt):
        pass

    def draw(self):
        if not self.hidden and not self.drawn:
            self.render_object.draw(window)
            self.drawn = True

    def undraw(self):
        if self.drawn:
            self.render_object.undraw()
            self.drawn = False


class PlaceholderRenderable(Renderable):
    def __init__(self):
        super().__init__(None)

    def draw(self):
        pass

    def undraw(self):
        pass


class DynamicRenderable(Renderable):
    def __init__(self, render_object, pos_x, pos_y, bb_left=0, bb_right=WINDOW_X, bb_up=0, bb_down=WINDOW_Y, sustain_oob=False):  # todo bounding box
        super().__init__(render_object)
        self.dx = 0
        self.dy = 0
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.bb_left = bb_left
        self.bb_right = bb_right
        self.bb_up = bb_up
        self.bb_down = bb_down
        self.sustain_oob = sustain_oob

    def oob(self):
        if self.pos_x > self.bb_right:
            return True
        elif self.pos_x < self.bb_left:
            return True
        elif self.pos_y > self.bb_down:
            return True
        elif self.pos_y < self.bb_up:
            return True
        return False


    def update(self, dt):  # override me, updating deltas rather than positions
        centre = self.render_object.getCenter()
        self.pos_x = centre.getX()
        self.pos_y = centre.getY()
        if not self.sustain_oob and self.oob():
            self.trash = True


    def draw(self):
        super().draw()
        if self.drawn:
            self.render_object.move(self.dx, self.dy)


class PlayerControlledRenderable(DynamicRenderable):
    def __init__(self, render_object, pos_x, pos_y,
                 base_speed=PLAYER_BASE_SPEED,
                 bound_x_left=BOX_TOP_LEFT_X, bound_x_right=BOX_BOTTOM_RIGHT_X,
                 bound_y_up=BOX_TOP_LEFT_Y, bound_y_down=BOX_BOTTOM_RIGHT_Y):
        super().__init__(render_object, pos_x, pos_y)
        self.speed = base_speed  # pixels/s
        self.dx = 0
        self.dy = 0
        self.bound_x_left = bound_x_left
        self.bound_x_right = bound_x_right
        self.bound_y_up = bound_y_up
        self.bound_y_down = bound_y_down

    def control(self, control_input, dt):
        delta_x = control_input.getX() * self.speed
        delta_y = -control_input.getY() * self.speed  # negative because of the coordinate system
        desired_x = self.pos_x + delta_x
        desired_y = self.pos_y + delta_y

        if not(desired_x > self.bound_x_right or desired_x < self.bound_x_left):  # there are problems here
            self.dx = delta_x
        else:
            if desired_x > self.bound_x_right:
                self.dx = self.bound_x_right - desired_x
            elif desired_x < self.bound_x_left:
                self.dx = self.bound_x_left - desired_x

        if not(desired_y < self.bound_y_up or desired_y > self.bound_y_down):
            self.dy = delta_y
        else:
            if desired_y > self.bound_y_down:
                self.dy = self.bound_y_down - desired_y
            elif desired_y < self.bound_y_up:
                self.dy = self.bound_y_up - desired_y

    def update(self, dt):  # control_input included here to prevent issues
        centre = self.render_object.getCenter()
        self.pos_x = centre.getX()
        self.pos_y = centre.getY()


class TextRenderable(DynamicRenderable):
    # these are done in tkinter directly because graphics.py doesn't do text well
    def __init__(self, string, top_left_x, top_right_y, fontsize=DEFAULT_FONT_SIZE, fontfamily=DEFAULT_FONT_FAMILY, colour=DEFAULT_TEXT_COLOUR):  # todo bounding box
        super().__init__(render_object=PlaceholderRenderable())
        self.dx = 0
        self.dy = 0
        self.pos_x = top_left_x
        self.pos_y = top_right_y
        self.string

    def oob(self):
        if self.pos_x > self.bb_right:
            return True
        elif self.pos_x < self.bb_left:
            return True
        elif self.pos_y > self.bb_down:
            return True
        elif self.pos_y < self.bb_up:
            return True
        return False


    def update(self, dt):  # override me, updating deltas rather than positions
        centre = self.render_object.getCenter()
        self.pos_x = centre.getX()
        self.pos_y = centre.getY()
        if not self.sustain_oob and self.oob():
            self.trash = True


    def draw(self):
        super().draw()
        if self.drawn:
            self.render_object.move(self.dx, self.dy)

class RenderableController(object):
    def __init__(self):
        self.static_renderables = list()
        self.dynamic_renderables = list()
        self.player_renderables = list()

        self.text_name_to_id = dict()
        self.text_renderables = dict()  # need to roll out dict with ID to all renderables
        self.pending_text_changes = dict()  # name: desired string

    def update(self, dt):
        trashlist = list()
        for i, x in enumerate(self.static_renderables):
            x.update(dt)
            if x.trash:
                trashlist.append(i)
        for x in trashlist[::-1]:  # iterate backwards so index doesn't break
            del self.static_renderables[x]
        trashlist = list()
        for i, x in enumerate(self.dynamic_renderables):
            x.update(dt)
            if x.trash:
                trashlist.append(i)
        for x in trashlist[::-1]:  # iterate backwards so index doesn't break
            del self.dynamic_renderables[x]
        for x in self.player_renderables:
            x.control(player_input, dt)
            x.update(dt)

        # if len(self.pending_text_changes):
        for k, v in self.pending_text_changes.items():
            window.itemconfigure(self.text_name_to_id[k], text=v)
        self.pending_text_changes = dict()


    def draw(self):
        for x in self.static_renderables:
            x.draw()
        for x in self.dynamic_renderables:
            x.draw()
        for x in self.player_renderables:
            x.draw()

    def undraw(self):
        for x in self.static_renderables:
            x.undraw()
        for x in self.dynamic_renderables:
            x.undraw()
        for x in self.player_renderables:
            x.undraw()

    def createAndLoadStaticRenderablesFromDictionary(self, graphics_dict):
        for v in graphics_dict.values():
            self.static_renderables.append(Renderable(v))

    def createAndLoadDynamicTextFromDictionary(self, id_to_text_dict):
        for k, v in id_to_text_dict.items():
            self.text_name_to_id[k] = window.create_text(v.pos_x, v.pos_y, fill=v.colour, text=v.text, anchor=v.anchor, font=v.font)

    def requestTextStringChange(self, text_name, desired_value):
        self.pending_text_changes[text_name] = desired_value



