import pygame
import globals


class SoundManager(object):
    def __init__(self):
        pygame.mixer.init()
        self.current_music = ""

    def playMusic(self, path):
        if globals.mute:
            return
        self.current_music = path
        pygame.mixer.music.load(path)
        pygame.mixer.music.play()
