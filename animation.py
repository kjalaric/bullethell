import os
import graphics
import renderables
from game import window


class Animation(renderables.Renderable):
    def __init__(self, base_dir, origin_x, origin_y, frame_delay):
        super().__init__(renderables.PlaceholderRenderable())
        self.images = list()
        self.origin = graphics.Point(origin_x, origin_y)
        self.frame = 0
        self.frame_delay_max = frame_delay
        self.frame_delay = 0
        for x in os.listdir(base_dir):  # i hope they're in order
            self.images.append(graphics.Image(self.origin, os.path.join(base_dir, x)))
            self.images[-1].undraw()
        self.images[self.frame].draw(window)
        self.drawn = False  # so it is always redrawn

    def _nextFrame(self):
        if self.frame < len(self.images)-1:
            self.frame += 1
            self.images[self.frame].draw(window)
            self.images[self.frame-1].undraw()
        else:
            self.frame = 0
            self.images[0].draw(window)
            self.images[-1].undraw()

    def draw(self):
        self.frame_delay += 1
        if self.frame_delay >= self.frame_delay_max:
            self.frame_delay = 0
            self._nextFrame()
