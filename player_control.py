class ControlInput(object):
    def __init__(self, left=False, right=False, up=False, down=False, escape=False, space=False, action1=False, action2=False, action3=False):
        self.left = left
        self.right = right
        self.up = up
        self.down = down
        self.esc = escape
        self.space = space
        self.action1 = action1
        self.action2 = action2
        self.action3 = action3

    def getX(self):  # accounts for diagonal
        if not self.up or self.down:
            if self.right:
                return 1
            elif self.left:
                return -1
            else:
                return 0
        else:
            if self.right:
                return 0.707
            elif self.left:
                return -0.707
            else:
                return 0

    def getY(self):
        if not self.right or self.left:
            if self.up:
                return 1
            elif self.down:
                return -1
            else:
                return 0
        else:
            if self.up:
                return 0.707
            elif self.down:
                return -0.707
            else:
                return 0

    def update(self, key_set):
        self.up = "w" in key_set
        self.down = "s" in key_set
        self.left = "a" in key_set
        self.right = "d" in key_set
        self.esc = "Escape" in key_set
        self.space = "<space>" in key_set
        self.action1 = "z" in key_set
        self.action2 = "x" in key_set
        self.action3 = "c" in key_set


    def debug(self):
        print(f"{self.up} | {self.down} | {self.left} | {self.right} | {self.space}")
