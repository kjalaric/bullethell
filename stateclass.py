import renderables
from globals import debug_panel_active
from debug import debug_panel


class State(object):
    def __init__(self, name=""):
        self.name = name
        self.renderable_controller = renderables.RenderableController()
        self.running = False  # changed once when the state is started

    def initialise(self):  # this can never have arguments
        pass

    def start(self):  # always call super from child classes
        self.running = True
        print(f"Starting State: {self.name}")

    def update(self, dt):
        self.renderable_controller.update(dt)

    def draw(self, *args, **kwargs):
        self.renderable_controller.draw()
        if debug_panel_active:
            debug_panel.update()

    def suspend(self, *args, **kwargs):
        self.running = False
        self.renderable_controller.undraw()

    def stop(self, *args, **kwargs):
        self.running = False

    def terminate(self, *args, **kwargs):
        pass
