import math
import graphics
import renderables
from defines import *


class Bullet(renderables.DynamicRenderable):
    def __init__(self, origin_x, origin_y, angle=0, speed=BULLET_SPEED_NORMAL, damage=0):  # angle in degrees
        render_object = graphics.Circle(graphics.Point(origin_x, origin_y), BULLET_SIZE_NORMAL)
        render_object.setOutline("red")
        render_object.setFill("red")
        super().__init__(render_object, origin_x, origin_y)
        self.speed = BULLET_SPEED_NORMAL
        self.active = True
        self.dx = speed*math.cos(math.radians(angle))
        self.dy = speed*math.sin(math.radians(angle))
        self.damage = damage
        if damage:
            self.damaging = True
        else:
            self.damaging = False

