import time
import graphics
import animation
import random
import bullets
import stateclass
import renderables
import box
from defines import *
from game import *
from visuals.player_bar import player_bar_graphics_items, player_bar_text_items


class DemoFightState(stateclass.State):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.animation_increased = False
        self.fight_start = None

    def initialise(self):
        window.setBackground("black")

        # player bar
        self.renderable_controller.createAndLoadStaticRenderablesFromDictionary(player_bar_graphics_items)
        self.renderable_controller.createAndLoadDynamicTextFromDictionary(player_bar_text_items)

        self.animation_increased = False
        self.fight_start = time.time()

        self.renderable_controller.static_renderables.append(renderables.Renderable(box.makeBox()))
        self.renderable_controller.player_renderables.append(renderables.PlayerControlledRenderable(box.makePlayer(BOX_CENTRE_X, BOX_CENTRE_Y), BOX_CENTRE_X, BOX_CENTRE_Y))
        self.renderable_controller.dynamic_renderables.append(bullets.Bullet(500, 100, 135))
        self.renderable_controller.static_renderables.append(animation.Animation("anim", BOX_CENTRE_X, 120, 2))

    def start(self, *args, **kwargs):
        print("Starting DEMO")
        soundmgr.playMusic("spooky.mp3")

    def update(self, dt):
        super().update(dt)

        # bullet spawn
        if time.time() - self.fight_start < 16:
            # less than 16s, slow
            if random.random() > 0.80:
                self.renderable_controller.dynamic_renderables.append(bullets.Bullet(random.randint(0, WINDOW_X), random.randint(10, WINDOW_BORDER_Y_TOP), random.randint(0, 180)))

        else:
            if not self.animation_increased:
                self.renderable_controller.static_renderables[-1].frame_delay_max = 1
                self.animation_increased = True

            # after 16s speed up
            if random.random() > 0.2:
                self.renderable_controller.dynamic_renderables.append(bullets.Bullet(random.randint(0, WINDOW_X), random.randint(10, WINDOW_BORDER_Y_TOP), random.randint(0, 180)))
                self.renderable_controller.dynamic_renderables.append(bullets.Bullet(random.randint(0, WINDOW_X), random.randint(10, WINDOW_BORDER_Y_TOP), random.randint(0, 180)))
                self.renderable_controller.dynamic_renderables.append(bullets.Bullet(random.randint(0, WINDOW_X), random.randint(10, WINDOW_BORDER_Y_TOP), random.randint(0, 180)))
                self.renderable_controller.dynamic_renderables.append(bullets.Bullet(random.randint(0, WINDOW_X), random.randint(10, WINDOW_BORDER_Y_TOP), random.randint(0, 180)))