import graphics
import stateclass
import renderables
from defines import *
import states.demo
from game import *


class MainMenuState(stateclass.State):
    def initialise(self):
        window.setBackground("black")

        main_text = graphics.Text(graphics.Point(BOX_CENTRE_X, 50), "This is definitely not undertale.")
        main_text.setSize(20)
        main_text.setTextColor("white")
        sub_text = graphics.Text(graphics.Point(BOX_CENTRE_X, 80), "Press SPACE to proceed.")
        sub_text.setSize(12)
        sub_text.setTextColor("white")

        self.renderable_controller.static_renderables.append(renderables.Renderable(main_text))
        self.renderable_controller.static_renderables.append(renderables.Renderable(sub_text))

    def update(self, dt):
        super().update(dt)
        statemgr.requestNewState(states.demo.DemoFightState("demo_fight"))