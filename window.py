import graphics  # pip install graphics.py


class Window(graphics.GraphWin):
    def __init__(self, title="Graphics Window", width=200, height=200, autoflush=True):
        super().__init__(title, width, height, autoflush)

        self.bind_all("<KeyPress>", self._onKeyPress)
        self.bind_all("<KeyRelease>", self._onKeyRelease)

        self.keys = set()

    def _onKeyPress(self, event):
        self.keys.add(event.keysym)
        print(event.keysym)

    def _onKeyRelease(self, event):
        self.keys.remove(event.keysym)
