class StateMachine(object):
    def __init__(self):
        self.state = list()  # this can be a LIFO stack
        self.requested_states = list() # this will be used as a FIFO queue

    def requestNewState(self, state):
        self.requested_states.append(state)

    def _updateStates(self):
        if not len(self.requested_states):
            return
        else:
            if len(self.state):
                self.state[-1].suspend()
            states_to_add = self.requested_states[::-1]
            self.requested_states = list()
            for x in states_to_add:
                self.state.append(x)
                self.state[-1].initialise()

            if len(self.requested_states):  # if new states added as a result of initialisation
                self.updateStates()

    def _updateCurrentState(self, dt):
        if not self.state[-1].running:
            self.state[-1].start()
            self.state[-1].running = True
        self.state[-1].update(dt)

    def _drawCurrentState(self):
        self.state[-1].draw()

    def run(self, dt):
        self._updateStates()
        self._updateCurrentState(dt)
        self._drawCurrentState()