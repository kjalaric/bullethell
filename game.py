from datetime import datetime
from defines import *
from window import Window
from state_machine import StateMachine
from player_control import ControlInput
from sound_manager import SoundManager

# classes
window = Window("Not Undertale", WINDOW_X, WINDOW_Y)
statemgr = StateMachine()
player_input = ControlInput()
soundmgr = SoundManager()

# technical
game_start = datetime.now()