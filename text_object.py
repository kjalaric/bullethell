from defines import DEFAULT_FONT_FAMILY, DEFAULT_FONT_SIZE, DEFAULT_TEXT_COLOUR
import tkinter as tk
import tkinter.font as tkFont


class Text(object):
    def __init__(self, pos_x=0, pos_y=0, text="", colour=DEFAULT_TEXT_COLOUR, font_family=DEFAULT_FONT_FAMILY, size=DEFAULT_FONT_SIZE, anchor=tk.NW):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.text = text
        self.colour = colour
        self.font = tkFont.Font(family=font_family, size=size)
        self.anchor = anchor
