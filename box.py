from graphics import *  # pip install graphics.py
from defines import *


def makeBox(origin_x=WINDOW_BORDER_X_LEFT, origin_y=WINDOW_BORDER_Y_TOP,
            size_x=WINDOW_BOX_X_SIZE, size_y=WINDOW_BOX_Y_SIZE,
            line_width=BOX_LINE_WIDTH, line_colour=BOX_LINE_COLOUR):
    box = Rectangle(Point(origin_x, origin_y), Point(origin_x+size_x, origin_y+size_y))
    box.setWidth(line_width)
    box.setOutline(line_colour)
    return box


def makePlayer(position_x, position_y, size=PLAYER_SIZE, colour=PLAYER_COLOUR):
    player = Circle(Point(position_x, position_y), size)
    player.setOutline(colour)
    player.setFill(colour)
    return player
