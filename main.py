import time
import states.main_menu
import states.demo
from game import *


if __name__ == "__main__":

    # start on menu
    statemgr.requestNewState(states.main_menu.MainMenuState("main_menu"))
    # statemgr.requestNewState(states.demo.DemoFightState("demo"))


    while True:
        # frame start
        frame_start = datetime.now()

        # update controller input
        player_input.update(window.keys)

        # run state machine
        statemgr.run(FRAME_PERIOD)

        # wait out the rest of the frame
        dt = datetime.now() - frame_start
        if (FRAME_PERIOD - dt.total_seconds()) > 0:
            time.sleep(FRAME_PERIOD - dt.total_seconds())  # might be more efficient to convert FRAME_PERIOD to a timedelta object



